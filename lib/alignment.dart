import 'package:flutter/material.dart';

void main() {
  runApp(const AlignExample());
}

class AlignExample extends StatelessWidget {
  const AlignExample({key}) : super(key: key);

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Container(
            height: 200.0,
            width: 600.0,
            color: Colors.red[200],
            child: Align(
              alignment: Alignment(-0.75, -0.75),
              child: Container(
                color: Colors.green[200],
                child: const Text('Align me!'),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
