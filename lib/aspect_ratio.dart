import 'package:flutter/material.dart';

class AspectRatioExample extends StatelessWidget {
  const AspectRatioExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 200.0,
        width: 600.0,
        color: Colors.red[200],
        child: Align(
          alignment: Alignment(-0.75, -0.75),
          child: Container(
            color: Colors.green[200],
            child: const Text('Align me!'),
          ),
        ),
      ),
    );
  }
}
